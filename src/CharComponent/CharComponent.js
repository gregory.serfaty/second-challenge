import React from 'react'

const style = {
    "display": "inline-block",
    "padding": "16px",
    "textAlign": "center",
    "margin": "16px",
    "border": "1px solid black"
}

const Char = props => {
    let textReceived = [...props.text];
    
    return <p style={style} onClick={props.click}>{textReceived}</p>
}


export default Char;