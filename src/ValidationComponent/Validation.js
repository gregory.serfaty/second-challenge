import React from 'react'

const style = {
    "border": "1px solid #ccc",
    "boxShadow": "0  2px 3px #ccc",
    "width": "300px",
    "height": "70px",
    "textAlign": "center",
    "paddingTop": "35px"
}

const Validation = props => {
    let textReturn = "";
    if (props.textLength > 0 && props.textLength < 5) {
        textReturn = 'Text too short';
    } else if (props.textLength > 16) {
        textReturn = 'Text long enough'
    }
    return (
        textReturn ? <p style={style}>{textReturn}</p>: null
    )
}

export default Validation;